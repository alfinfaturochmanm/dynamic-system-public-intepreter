-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for peugeot_indonesia
DROP DATABASE IF EXISTS `peugeot_indonesia`;
CREATE DATABASE IF NOT EXISTS `peugeot_indonesia` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `peugeot_indonesia`;

-- Dumping structure for table peugeot_indonesia.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.components
DROP TABLE IF EXISTS `components`;
CREATE TABLE IF NOT EXISTS `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `library_component` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_component` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT 0,
  `grids` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.components: ~43 rows (approximately)
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
INSERT INTO `components` (`id`, `html_id`, `html_class`, `content`, `library_component`, `type_component`, `sequence`, `grids`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'sliderHome', '{"0":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUV"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"1":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUS"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"2":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUR"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"3":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}}}', 'slider-5', 6, 1, 1, NULL, NULL),
	(2, NULL, NULL, '{"src":"whatsapp-badge.png"}', NULL, 4, 1, 2, NULL, NULL),
	(3, 'sliderProductPreview', 'sliderProductPreview', '{"0":{"src":"","href":"","heading":[{"tag":"h3","text":"3008 SUV"}],"description":[{"tag":"h6","text":"AVAILABLE IN"},{"tag":"h6","text":""},{"tag":"h6","text":"*OTR DKI JAKARTA"},{"tag":"h6","text":"730.000.000 IDR"}],"additional":{"slider":[{"indicator":"Copper Metalic","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Ultimate Red","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Perlescent White","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nera Black","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nimbus Grey","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_002.png","text":"*tampak belakang"}]}]}},"1":{"src":"","href":"","heading":[{"tag":"p","text":"5008 SUV"}],"description":[{"tag":"p","text":"AVAILABLE IN"},{"tag":"h6","text":""},{"tag":"p","text":"*OTR DKI JAKARTA"},{"tag":"p","text":"730.000.000 IDR"}],"additional":{"slider":[{"indicator":"Copper Metalic","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Ultimate Red","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Perlescent White","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nera Black","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nimbus Grey","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_002.png","text":"*tampak belakang"}]}]}}}', 'slider-6', 6, 1, 3, NULL, NULL),
	(4, NULL, 'vidioPlayer', '{"src":"https://youtu.be/R2LQdh42neg","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', NULL, 5, 1, 8, NULL, NULL),
	(5, NULL, NULL, '{"type":"h2","value":"PRODUCT"}', NULL, 2, 1, 9, NULL, NULL),
	(6, NULL, NULL, '{"type":"h2","value":"VIDEO"}', NULL, 2, 2, 9, NULL, NULL),
	(7, NULL, NULL, '{"type":"h3","value":"FIVE YEARS PEACE OF MIND"}', NULL, 2, 1, 10, NULL, NULL),
	(8, NULL, NULL, '{"src":"Logo 24hr Roadside Assistance.png","href":"google.com"}', NULL, 4, 1, 11, NULL, NULL),
	(9, NULL, NULL, '{"type":"p","value":"Layanan 24 jam untuk menangani kendaraan dalam kondisi darurat selama 5 tahun oleh Astraworld"}', NULL, 3, 2, 11, NULL, NULL),
	(10, NULL, NULL, '{"src":"Logo Home Service.png","href":"google.com"}', NULL, 4, 1, 12, NULL, NULL),
	(11, NULL, NULL, '{"type":"p","value":"Layanan servis kendaraan di lokasi yang telah disepakati dengan customer"}', NULL, 3, 2, 12, NULL, NULL),
	(12, NULL, NULL, '{"src":"Logo PMP.png","href":"google.com"}', NULL, 4, 1, 13, NULL, NULL),
	(13, NULL, NULL, '{"type":"p","value":"Free Servis berkala selama 5 tahun atau 60.000 km (mana yang dicapai terlebih dahulu)"}', NULL, 3, 2, 13, NULL, NULL),
	(14, NULL, NULL, '{"src":"Logo Warranty.png","href":"google.com"}', NULL, 4, 1, 14, NULL, NULL),
	(15, NULL, NULL, '{"type":"p","value":"Jaminan garansi kendaraan selama 5 tahun atau 100.000 km (mana yang dicapai terlebih dahulu)"}', NULL, 3, 2, 14, NULL, NULL),
	(16, NULL, NULL, '{"type":"h3","value":"PEUGEOT BODY & PAINT"}', NULL, 2, 1, 18, NULL, NULL),
	(17, NULL, NULL, '{"type":"p","value":"Body & Paint Astra Peugeot merupakan satu-satunya bengkel authorized body repair yang khusus menangani perbaikan body dan pengecatan kendaraan bermotor yang dilengkapi peralatan sesuai standar fasilitas yang ditetapkan oleh Peugeot."}', NULL, 3, 1, 19, NULL, NULL),
	(18, NULL, NULL, '{"type":"p","value":"Bengkel Body & Paint Astra Peugeot juga melayani seluruh merk kendaraan selain merk Peugeot."}', NULL, 3, 1, 20, NULL, NULL),
	(19, NULL, NULL, '{"type":"p","value":"Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 21, NULL, NULL),
	(20, NULL, NULL, '{"type":"p","value":"See the location on maps <a class=\'buttonLink-section-body-paint\'  href=\'\'><span class=\'fa fa-chevron-right\'></a>"}', NULL, 3, 2, 21, NULL, NULL),
	(21, NULL, NULL, '{"type":"h2","value":"PRODUCT"}', NULL, 2, 1, 15, NULL, NULL),
	(22, NULL, 'section_preview_nav active', '{"href":"javascript:void(0)","value":"3008 SUV"}', NULL, 7, 1, 16, NULL, NULL),
	(23, NULL, 'section_preview_nav', '{"href":"javascript:void(0)","value":"5008 SUV"}', NULL, 7, 2, 16, NULL, NULL),
	(24, NULL, 'section_preview_slider_tab active', '{"href":"javascript:void(0)","value":"Exterior"}', NULL, 7, 1, 17, NULL, NULL),
	(25, NULL, 'section_preview_slider_tab', '{"href":"javascript:void(0)","value":"Interior"}', NULL, 7, 2, 17, NULL, NULL),
	(26, 'sliderProductNavigate', 'sliderProductNavigate', '{"0":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUV"}]},"1":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUS"}]},"2":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUR"}]},"3":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]},"4":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]},"5":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]},"6":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]}}', 'slider-7', 6, 1, 23, NULL, NULL),
	(27, NULL, NULL, '{"type":"h2","value":"DEALER"}', NULL, 2, 1, 24, NULL, NULL),
	(28, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>ASTRA PEUGEOT SUNTER<br> JL YOS SUDARSO KAV 24 SUNTER II SUNTER 14330 NORTH JAKARTA<br> 0062216512325"}', NULL, 3, 1, 25, NULL, NULL),
	(29, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 26, NULL, NULL),
	(30, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 27, NULL, NULL),
	(31, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 28, NULL, NULL),
	(32, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 29, NULL, NULL),
	(33, NULL, NULL, '{"type":"h2","value":"EXIBITION"}', NULL, 2, 1, 30, NULL, NULL),
	(34, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>ASTRA PEUGEOT SUNTER<br> JL YOS SUDARSO KAV 24 SUNTER II SUNTER 14330 NORTH JAKARTA<br> 0062216512325"}', NULL, 3, 2, 30, NULL, NULL),
	(35, NULL, NULL, '{"type":"p","value":"See the location on maps <a class=\'buttonLink-section-exibition\'  href=\'\'><span class=\'fa fa-chevron-right\'></a>"}', NULL, 3, 3, 30, NULL, NULL),
	(36, NULL, NULL, '{"src":"mapsLocation.png","href":"google.com"}', NULL, 4, 1, 31, NULL, NULL),
	(37, NULL, NULL, '{"type":"h2","value":"Event & Promotion"}', NULL, 2, 1, 32, NULL, NULL),
	(38, NULL, NULL, '{"type":"p","value":"Latest News"}', NULL, 3, 1, 33, NULL, NULL),
	(39, NULL, NULL, '{"type":"p","value":"Other News"}', NULL, 3, 1, 34, NULL, NULL),
	(40, NULL, NULL, '{"src":"news1.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<button>Read More</button>"]}', NULL, 12, 1, 35, NULL, NULL),
	(41, NULL, NULL, '{"src":"news2.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"NEW PEUGEOT 5008 PREMIUM SUV 7–SEATER RESMI MELUNCUR","additional":["Astra International Tbk – Peugeot Sales Operation (Astra Peugeot) selaku Agen Pemegang Merek dan Distributor Peugeot di Indonesia meluncurkan New Peugeot.","<button>Read More</button>"]}', NULL, 12, 2, 35, NULL, NULL),
	(42, NULL, NULL, '{"src":"news3.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"LOREM IPSUM DOLOR SIT AMET","additional":["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,","<button>Read More</button>"]}', NULL, 12, 3, 35, NULL, NULL),
	(43, NULL, NULL, '{"src":"news4.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"LOREM IPSUM DOLOR SIT AMET","additional":["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,","<button>Read More</button>"]}', NULL, 12, 4, 35, NULL, NULL);
/*!40000 ALTER TABLE `components` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.css
DROP TABLE IF EXISTS `css`;
CREATE TABLE IF NOT EXISTS `css` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projects` int(11) NOT NULL DEFAULT 0,
  `code` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.css: ~0 rows (approximately)
/*!40000 ALTER TABLE `css` DISABLE KEYS */;
INSERT INTO `css` (`id`, `projects`, `code`, `created_at`, `updated_at`) VALUES
	(1, 1, '', '2019-10-22 11:58:36', '2019-10-22 11:58:36');
/*!40000 ALTER TABLE `css` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.css_responsive
DROP TABLE IF EXISTS `css_responsive`;
CREATE TABLE IF NOT EXISTS `css_responsive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `css` int(11) NOT NULL DEFAULT 0,
  `code` text NOT NULL,
  `max-width` int(11) NOT NULL DEFAULT 0,
  `min-width` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.css_responsive: ~0 rows (approximately)
/*!40000 ALTER TABLE `css_responsive` DISABLE KEYS */;
/*!40000 ALTER TABLE `css_responsive` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.css_style_guide
DROP TABLE IF EXISTS `css_style_guide`;
CREATE TABLE IF NOT EXISTS `css_style_guide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_component` int(11) NOT NULL DEFAULT 0,
  `tag` varchar(10) NOT NULL DEFAULT '0',
  `class` varchar(50) NOT NULL DEFAULT '0',
  `code` text NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.css_style_guide: ~5 rows (approximately)
/*!40000 ALTER TABLE `css_style_guide` DISABLE KEYS */;
INSERT INTO `css_style_guide` (`id`, `type_component`, `tag`, `class`, `code`, `created_at`, `updated_at`) VALUES
	(1, 2, 'h1', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(2, 2, 'h2', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(3, 2, 'h3', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(4, 2, 'h4', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(5, 3, 'p', '_font_arial', 'font-family: arial !important;', NULL, NULL);
/*!40000 ALTER TABLE `css_style_guide` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.grids
DROP TABLE IF EXISTS `grids`;
CREATE TABLE IF NOT EXISTS `grids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `length` tinyint(4) NOT NULL,
  `sequence` int(11) NOT NULL,
  `sections` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.grids: ~30 rows (approximately)
/*!40000 ALTER TABLE `grids` DISABLE KEYS */;
INSERT INTO `grids` (`id`, `html_id`, `html_class`, `length`, `sequence`, `sections`, `created_at`, `updated_at`) VALUES
	(1, NULL, NULL, 12, 1, 1, NULL, NULL),
	(2, NULL, '_grid-badge-whatsapp', 1, 1, 1, NULL, NULL),
	(3, NULL, ' slider-preview tab-active', 12, 4, 2, NULL, NULL),
	(8, NULL, NULL, 8, 1, 3, NULL, NULL),
	(9, NULL, NULL, 4, 2, 3, NULL, NULL),
	(10, NULL, NULL, 12, 1, 4, NULL, NULL),
	(11, NULL, NULL, 3, 2, 4, NULL, NULL),
	(12, NULL, NULL, 3, 3, 4, NULL, NULL),
	(13, NULL, NULL, 3, 4, 4, NULL, NULL),
	(14, NULL, NULL, 3, 5, 4, NULL, NULL),
	(15, NULL, NULL, 12, 1, 2, NULL, NULL),
	(16, NULL, NULL, 6, 2, 2, NULL, NULL),
	(17, NULL, NULL, 6, 3, 2, NULL, NULL),
	(18, NULL, NULL, 12, 1, 5, NULL, NULL),
	(19, NULL, NULL, 12, 2, 5, NULL, NULL),
	(20, NULL, NULL, 12, 3, 5, NULL, NULL),
	(21, NULL, NULL, 4, 4, 5, NULL, NULL),
	(23, NULL, ' slider-preview', 12, 5, 2, NULL, NULL),
	(24, NULL, NULL, 4, 1, 6, NULL, NULL),
	(25, NULL, NULL, 4, 2, 6, NULL, NULL),
	(26, NULL, NULL, 4, 3, 6, NULL, NULL),
	(27, NULL, NULL, 4, 4, 6, NULL, NULL),
	(28, NULL, NULL, 4, 5, 6, NULL, NULL),
	(29, NULL, NULL, 4, 6, 6, NULL, NULL),
	(30, NULL, NULL, 6, 1, 7, NULL, NULL),
	(31, NULL, NULL, 6, 2, 7, NULL, NULL),
	(32, NULL, NULL, 12, 1, 8, NULL, NULL),
	(33, NULL, NULL, 6, 2, 8, NULL, NULL),
	(34, NULL, NULL, 6, 3, 8, NULL, NULL),
	(35, NULL, NULL, 12, 4, 8, NULL, NULL);
/*!40000 ALTER TABLE `grids` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.groups
DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.groups: ~3 rows (approximately)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'test group 1 updated', '2019-09-24 08:44:41', '2019-09-24 08:46:28'),
	(4, 'test', '2019-10-02 08:40:36', '2019-10-02 08:40:36'),
	(5, 'test', '2019-10-02 08:49:50', '2019-10-02 08:49:50');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.library_components
DROP TABLE IF EXISTS `library_components`;
CREATE TABLE IF NOT EXISTS `library_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `css` text DEFAULT NULL,
  `javascript` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.library_components: ~10 rows (approximately)
/*!40000 ALTER TABLE `library_components` DISABLE KEYS */;
INSERT INTO `library_components` (`id`, `name`, `css`, `javascript`, `created_at`, `updated_at`) VALUES
	(1, 'bootstrap_carousel_type_1', '.bootstrap_carousel_type_1 .carousel .carousel-control-next{\r\nposition: absolute;\r\nbottom: 10px;\r\nright: unset;\r\nleft: 38%;\r\nborder: 2px solid #2679BF;\r\npadding: 10px;\r\nwidth: 33px;\r\nheight: 33px;\r\nborder-radius: 15pc;\r\ntop: unset;\r\nbackground-color: transparent;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-control-next .carousel-control-next-icon\r\n{\r\n  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3E%3Cpath d=\'M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z\'/%3E%3C/svg%3E")!important;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-control-prev .carousel-control-prev-icon\r\n{\r\n  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3E%3Cpath d=\'M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z\'/%3E%3C/svg%3E");!important;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-control-prev{\r\nposition: absolute;\r\nbottom: 10px;\r\nright: 65%;\r\nborder: 2px solid #2679BF;\r\npadding: 10px;\r\nwidth: 33px;\r\nheight: 33px;\r\nborder-radius: 15pc;\r\ntop: unset;\r\nleft: unset;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-caption{\r\nvisibility:hidden;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-indicators{\r\nvisibility : hidden;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-item{\r\npadding-left: 15%;\r\nheight: 250px;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-item img{\r\n    right: 0;\r\n    width: 50% !important;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-item .carousel-caption.d-none.d-md-block h5 {\r\n    visibility: visible;\r\n    position: absolute;\r\n    top: -180;\r\n    left: -60;\r\n    color: black;\r\n}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'bootstrap_carousel_type_2', '.bootstrap_carousel_type_2 .carousel .carousel-indicators {\r\n    position: absolute;\r\n    right: 25;\r\n    bottom: 35%;\r\n    top: 35%;\r\n    z-index: 15;\r\n    justify-content: center;\r\n    list-style: none;\r\n    float: unset;\r\n    left: unset;\r\n    display: unset;\r\n    padding: unset;\r\n    margin: unset;\r\n}\r\n\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators li {\r\n    box-sizing: content-box;\r\n    width: 10px;\r\n    height: 10px;\r\n    cursor: pointer;\r\n    background-color: #fff;\r\n    background-clip: padding-box;\r\n    margin: 15px;\r\n    border-radius: 15px;\r\n    flex: unset;\r\n    text-indent: unset;\r\n    border: unset;\r\n    transition: unset;\r\n    opacity: unset;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators {\r\n    visibility: visible;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-prev {\r\nposition: absolute;\r\n    bottom: 130px;\r\n    right: 60px;\r\n    border: 2px solid #2679BF;\r\n    padding: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 15pc;\r\n    top: unset;\r\n    left: unset;\r\n    opacity:unset;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-next {\r\n    background-color: transparent;\r\nposition: absolute;\r\n    bottom: 130px;\r\n    right: 15px;\r\n    border: 2px solid #2679BF;\r\n    padding: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 15pc;\r\n    top: unset;\r\n    opacity:unset;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-prev .carousel-control-prev-icon {\r\n    background-image: url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3e%3cpath d=\'M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z\'/%3e%3c/svg%3e") !important;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-next .carousel-control-next-icon {\r\n    background-image: url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3e%3cpath d=\'M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z\'/%3e%3c/svg%3e") !important;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators{\r\nopacity:1!important;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators .active{\r\nopacity:1!important;\r\nbackground-color:#2679BF;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-inner .carousel-item .carousel-caption{\r\nposition: absolute;\r\n    right: unset;\r\n    bottom: 0;\r\n    left: unset;\r\n    z-index: 10;\r\n    padding-top: 20px;\r\n    color: #fff;\r\n    text-align: center;\r\n    background-color: rgba(0,0,0,0.5);\r\n    width: 100%;\r\n    height: 120px;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-inner .carousel-item .carousel-caption h5{\r\ndisplay:none;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-inner .carousel-item .carousel-caption p{\r\ntext-align: left;\r\n    padding: 20px;\r\n    margin-top: -20px;\r\n    font-size: 13px;\r\n}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'bootstrap_carousel_type_3', '.bootstrap_carousel_type_3 .carousel .carousel-indicators{\r\nvisibility:visible;\r\nbottom:20%;\r\nopacity:1!important;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-indicators li{\r\nwidth: 8px;\r\n    height: 8px;\r\n    border: unset;\r\n    border-radius: 15pc;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators .active{\r\nopacity:1!important;\r\nbackground-color:#2679BF;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-next:hover{\r\nopacity:1 !important;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-prev:hover{\r\nopacity:1 !important;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-next{\r\nright: unset;\r\n    border: unset;\r\n    padding: unset;\r\n    height: unset;\r\n    border-radius: unset;\r\n    background-color: transparent;\r\nposition: absolute;\r\n    top: 0;\r\n    bottom: 0;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n    -ms-flex-align: center;\r\n    align-items: center;\r\n    -webkit-box-pack: center;\r\n    -ms-flex-pack: center;\r\n    justify-content: center;\r\n    width: 15%;\r\n    color: #fff;\r\n    text-align: center;\r\n    opacity: .5;\r\nborder: unset;\r\n    padding: unset;\r\n    height: 50px;\r\n    width: 50px;\r\n    left: 93%;\r\n    right: unset;\r\n    border: 2px solid;\r\n    border-radius: 15pc;\r\n    margin: auto 10px;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-prev{\r\nright: unset;\r\n    border: unset;\r\n    padding: unset;\r\n    height: unset;\r\n    border-radius: unset;\r\n    background-color: transparent;\r\nposition: absolute;\r\n    top: 0;\r\n    bottom: 0;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n    -ms-flex-align: center;\r\n    align-items: center;\r\n    -webkit-box-pack: center;\r\n    -ms-flex-pack: center;\r\n    justify-content: center;\r\n    width: 15%;\r\n    color: #fff;\r\n    text-align: center;\r\n    opacity: .5;\r\nborder: unset;\r\n    padding: unset;\r\n    height: 50px;\r\n    width: 50px;\r\n    left: unset;\r\n    right: unset;\r\n    border: 2px solid;\r\n    border-radius: 15pc;\r\n    margin: auto 10px;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-inner .carousel-item .carousel-caption {\r\nposition: absolute;\r\n    right: unset;\r\n    bottom: 0;\r\n    left: unset;\r\n    z-index: 10;\r\n    padding-top: 20px;\r\n    color: #fff;\r\n    text-align: center;\r\n    background-color: rgba(0,0,0,0.5);\r\n    width: 100%;\r\n    height: 120px;\r\n}\r\n\r\n.bootstrap_carousel_type_3 .carousel .carousel-inner .carousel-item .carousel-caption h5{\r\ndisplay:none;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-inner .carousel-item .carousel-caption p{\r\ntext-align: left;\r\n    padding: 20px;\r\n    margin-top: -20px;\r\n    font-size: 13px;\r\n}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 'slider-1', '.slider-1{\r\nheight: 650px;\r\n}\r\n.slider-1 .slider-item-container {\r\ndisplay: table;\r\nwidth:100%;\r\n}\r\n.slider-1 .slider-item-container .slider-item{\r\n    display: table-cell;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(1) {\r\n    top: 150px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(2) {\r\n    top:100px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(3) {\r\n    top:50px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(4) {\r\n    top:25px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(5) {\r\n    top:50px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(6) {\r\n    top:100px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(7) {\r\n    top:150px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item img {\r\n    width: 80px;\r\n    height: 80px;\r\n    object-fit: cover;\r\n    object-position: center;\r\n    border-radius: 200px;\r\n    margin: 0px auto;\r\n    display: block;\r\n    cursor: pointer;\r\n    position: relative;\r\n    border: 1px solid black;\r\n    border-radius: 15pc;\r\n    padding: 20px;\r\n}\r\n.slider-1 .slider-item-container .slider-item .slider-heading{\r\n    position: relative;\r\n    top: 40%;\r\n    font-family: \'Permanent Marker\', cursive;\r\n    text-align: center;\r\n    color: white;\r\n    display : none;\r\n}\r\n.slider-1 .slider-item-container .slider-item .slider-description{\r\n    position: relative;\r\n    padding-left: 35%;\r\n    color: white;\r\n    top: 35%;\r\n    display : none;\r\n}\r\n.slider-1 .preview-img {\r\n    width: 100%;\r\n    height: 400px;\r\n    margin-bottom:40px;\r\n\r\n}\r\n.slider-1 .preview-img img{\r\n    width: 30%;\r\n    object-fit: cover;\r\n    margin-left: 35%;\r\n    margin-right: 40%;\r\n    margin-top: 20px;\r\n}\r\n.slider-1 .preview-img .caption{\r\nmargin-top:80px;\r\n}\r\n.slider-1 .preview-img .caption h5{\r\n    visibility: visible;\r\n    color: black;\r\n    position: absolute;\r\n}\r\n.slider-1 .preview-img .caption p{\r\n    color: black;\r\n    position: absolute;\r\n    margin-top: 30px;\r\n    width: 100%;\r\n}', '<script type="text/javascript">\r\nvar elitem = $(\'.slider-1 .slider-item-container .slider-item\');\r\nvar elActive = $(\'.slider-1 .slider-item-container .active\');\r\nvar img = elActive.find(\'img\').attr(\'src\');\r\nvar heading = elActive.find(\'.slider-heading\').text();\r\nvar description = elActive.find(\'.slider-description\').text();\r\nconsole.log(elitem);\r\nconsole.log(heading);\r\nconsole.log(description);\r\nvar previewImg = \'<div class="preview-img"><img src="\' + img + \'"><div class="caption"><h5 class="slider-heading">\'+heading+\'</h5><p>\'+description+\'</p></div></div>\';\r\n$(previewImg).insertAfter($(\'.slider-1 .slider-item-container\'));\r\nelitem.click(function(){\r\n$(\'.preview-img img\').attr(\'src\', $(this).children(\'img\').attr(\'src\'));\r\nvar index =  $(this).children(\'h5\').text();\r\nvar indux =  $(this).children(\'p\').text();\r\nconsole.log(indux);\r\n$(\'.preview-img h5\').text(index);\r\n$(\'.preview-img p\').text(indux);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 'slider-2', '.slider-2 .slider-item-container {\r\ndisplay: table;\r\nwidth:100%;\r\n}\r\n.slider-2 .slider-item-container .slider-item {\r\n    display: table-cell;\r\n    height: 200px;\r\n    position: relative;\r\n    cursor: pointer;\r\n    width: 320px;\r\n}\r\n.slider-2 .slider-item-container .slider-item img {\r\n    width: 100%;\r\n    height: 200px;\r\n    position: absolute;\r\n    object-fit: cover;\r\n    object-position: center;\r\n    background-color: #fff;\r\n}\r\n.slider-2 .slider-item-container .slider-item .slider-heading{\r\n    position: relative;\r\n    top: 40%;\r\n    font-family: \'Permanent Marker\', cursive;\r\n    text-align: center;\r\n    color: black;\r\n}\r\n.slider-2 .slider-item-container .slider-item .slider-description{\r\nposition: relative;\r\n    color: black;\r\n    top: 35%;\r\n    text-align: center;\r\n}\r\n.slider-2 .slider-item-container .active .slider-heading{\r\n    color: white !important;\r\n}\r\n.slider-2 .slider-item-container .active .slider-description{\r\n    color: white !important;\r\n}\r\n.slider-2 .preview-img {\r\n    width: 100%;\r\n    margin-bottom:40px;\r\n}\r\n.slider-2 .preview-img img{\r\n    width: 100%;\r\n    height: 500px;\r\n    object-fit: cover;\r\n    object-position: center;\r\n}\r\n\r\n.slider-2 .preview-img h5{\r\n  top: 0;\r\n    margin: 200px 0px 0px 30px;\r\n    font-family: \'Permanent Marker\', cursive;\r\n    color: white;\r\n    position: absolute;\r\n}\r\n\r\n.slider-2 .preview-img p{\r\n    margin: 230px 0px 0px 30px;\r\n    color: white;\r\n    top: 0;\r\n    position: absolute;\r\n}\r\n.slider-2 .slider-item-container {\r\n    display: table;\r\n    width: 100%;\r\n    position: absolute;\r\n    bottom: 50;\r\n}\r\n.slider-2 .slider-item-container .active {\r\n    bottom: 20px;\r\n}\r\n.slider-2 .slider-item-container .slider-item::after {\r\n        content: \'\';\r\n    position: absolute;\r\n    left: 0;\r\n    bottom: -1px;\r\n    right: 0;\r\n    height: 30%;\r\n    background: linear-gradient(to bottom,rgba(255,255,255,0),#ccc) repeat left top;\r\n}\r\n.slider-2 .slider-item-container .active::after {\r\n        content: \'\';\r\n    position: absolute;\r\n    left: 0;\r\n    bottom: -1px;\r\n    right: 0;\r\n    height: 30%;\r\n    background: linear-gradient(to bottom,rgba(255,255,255,0),#113945) repeat left top;\r\n}', '<script type="text/javascript">\r\nvar elitem = $(\'.slider-2 .slider-item-container .slider-item\');\r\nvar elActive = $(\'.slider-2 .slider-item-container .active\');\r\nvar img = elActive.find(\'img\').attr(\'src\');\r\nvar heading = elActive.find(\'.slider-heading\').text();\r\nvar description = elActive.find(\'.slider-description\').text();\r\nconsole.log(elitem);\r\nconsole.log(heading);\r\nconsole.log(description);\r\nvar previewImg = \'<div class="preview-img"><img src="\' + img + \'"><h5 class="slider-heading">\'+heading+\'</h5><p>\'+description+\'</p></div>\';\r\n$(previewImg).insertBefore($(\'.slider-2 .slider-item-container\'));\r\nelitem.click(function(){\r\nelitem.removeClass(\'active\');\r\n$(this).addClass(\'active\');\r\n$(\'.preview-img img\').attr(\'src\', $(this).children(\'img\').attr(\'src\'));\r\nvar index =  $(this).children(\'h5\').text();\r\nvar indux =  $(this).children(\'p\').text();\r\nconsole.log(indux);\r\n$(\'.preview-img h5\').text(index);\r\n$(\'.preview-img p\').text(indux);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 'slider-3', '.slider-3{\r\nmargin-top:20px;\r\n}\r\n\r\n.slider-3 .slider-item-container {\r\ndisplay: table;    \r\nwidth: 75%;\r\n    margin: 0px auto;\r\n}\r\n\r\n.slider-item{\r\ndisplay: table-cell;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item img{\r\nwidth: 126px;\r\n    height: 126px;\r\n    border-radius: 15pc;\r\ncursor:pointer;\r\ndisplay:block;\r\nmargin:0px auto;\r\nborder: 1px solid;\r\nbackground-color:#fff;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item img:first-child, .slider-3 .slider-item-container .slider-item img:last-child{\r\nz-index:6;\r\nleft:40%;\r\nright:40%;\r\n}\r\n.slider-3 .slider-item-container .slider-item img:nth-child(2), .slider-3 .slider-item-container .slider-item img:nth-child(4){\r\nz-index:9;\r\n}\r\n.slider-3 .slider-item-container .slider-item:first-child{\r\nwidth: 130px;\r\n    height: 130px;\r\n    left: 20%;\r\n    position: absolute;\r\n}\r\n.slider-3 .slider-item-container .slider-item:nth-child(2){\r\nposition: absolute;\r\n    left: 30%;\r\n    width: 130px;\r\n    height: 130px;\r\n}\r\n.slider-3 .slider-item-container .slider-item:nth-child(4){\r\n    position: absolute;\r\n    right: 30%;\r\n    width: 130px;\r\n    height: 130px;\r\nz-index:9;\r\n}\r\n.slider-3 .slider-item-container .slider-item:last-child{\r\nwidth: 130px;\r\n    height: 130px;\r\n    right: 20%;\r\n    position: absolute;\r\nz-index:6;\r\n}\r\n.slider-3 .slider-item-container .active img{\r\nwidth: 200px;\r\n    height: 200px;\r\ndisplay:block;\r\nmargin:0px auto;\r\nposition: absolute;\r\n    top: 0;\r\nz-index:10!important;\r\n}\r\n\r\n.slider-3 .slider-item-container .active h5 {\r\n    visibility: visible!important;\r\ntext-align: center;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item h5{\r\nvisibility:hidden;\r\nmargin-top: 200px;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item p{\r\nvisibility:hidden!important;\r\n}', '<script type="text/javascript">\r\nvar switchActive = $(\'.slider-3 .slider-item-container .slider-item:nth-child(3)\');\r\nvar itemActive = $(\'.slider-3 .slider-item-container .active\');\r\nitemActive.removeClass(\'active\');\r\nswitchActive.addClass(\'active\');\r\nvar itemClick = $(\'.slider-3 .slider-item-container .slider-item\');\r\nitemClick.click(function(){\r\nvar Active = $(\'.slider-3 .slider-item-container .active\');\r\nvar src = $(this).find(\'img\');\r\nvar image = src.attr(\'src\');\r\nvar srcActive = Active.find(\'img\');\r\nvar imageActive = srcActive.attr(\'src\');\r\nvar heading = $(this).find(\'h5\').text();\r\nvar headingActive = Active.find(\'h5\').text();\r\nconsole.log(heading);\r\nconsole.log(headingActive);\r\nsrc.attr(\'src\', imageActive);\r\nsrcActive.attr(\'src\', image);\r\nsrc.attr(\'src\', imageActive);\r\n$(this).find(\'h5\').html(headingActive);\r\nActive.find(\'h5\').html(heading);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 'slider-4', 'h2{\r\n   font-size:35px;\r\n   position: absolute;\r\n    top: 5px;\r\n    left: 41%;\r\n}\r\n.slider-4 .slider-item-container .slider-item h2 {\r\n    display: none;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active h2 {\r\n    display: block;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active {\r\nwidth: 40%;\r\n    height: 465px;\r\n    position: inherit;\r\n    object-fit: cover;\r\n    cursor:unset;\r\n}\r\n.slider-4 .slider-item-container .slider-item{\r\n    width: 18%;\r\n    height: 100px;\r\n    float: left;\r\n    display: table-cell;\r\n    position: relative;\r\n    top: 365px;\r\n    margin: 0px 10px 0px 0px;\r\n    cursor:pointer;\r\n}\r\n.slider-4 .slider-item-container .slider-item img {\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slider-4 .slider-item-container .slider-item p {\r\n    position: absolute;\r\n    top: 20%;\r\n    left: 41%;\r\n   display: none;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active p {\r\n    position: absolute;\r\n    top: 20%;\r\n    left: 41%;\r\n    display: block !important;\r\n   font-size:20px;\r\n}\r\n.slider-4 .slider-item-container .slider-item h5 {\r\n    position: absolute;\r\n    top: 10%;\r\n    left: 41%;\r\n   display: none;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active h5 {\r\n    position: absolute;\r\n    top: 10%;\r\n    left: 41%;\r\n   display: block !Important;\r\n   font-size:20px;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active img{\r\n    width: 100%;\r\n    height: 100%;\r\n    object-fit: cover;\r\n}\r\n.slider-4 .slider-item-container .slider-item:before {\r\n  content: \'\';\r\n  display: block;\r\n  position:absolute;\r\n  width:100%;\r\n  transition: height 0.5s ease-out;\r\n  background:rgba(0,0,0,0.3);\r\n}\r\n.slider-4 .slider-item-container .slider-item:hover:before {\r\n  height: 100%;\r\n}\r\n.slider-4 .slider-item-container .active:before {\r\n  content: \'\';\r\n  display: none;\r\n  position:absolute;\r\n  width:100%;\r\n  transition: height 0.5s ease-out;\r\n  background:rgba(0,0,0,0.3);\r\n}\r\n.slider-4 .slider-item-container .active:hover:before {\r\n  height: 100%;\r\n}\r\n.slider-4 .slider-item-container .slider-item:hover .slider-heading{\r\ndisplay: block !important;\r\nopacity:1 !important;\r\n    color: white;\r\n    left: unset;\r\n    top: 0;\r\n    width: 100%;\r\n    text-align: center;\r\n    margin-top: 40px;\r\n    font-size: 16px;\r\n}\r\n.slider-4 .slider-item-container .active:hover .slider-heading{\r\nopacity:1 !important;\r\n    color: black;\r\n    left: unset;\r\n    top: unset;\r\n    width: unset;\r\n    text-align: unset;\r\n    margin-top: unset;\r\n    font-size: unset;\r\n    position: absolute;\r\n    top: 10%;\r\n    left: 41%;\r\n   display: block !Important;\r\n   font-size:20px;\r\n}', '<script type="text/javascript">\r\n$(\'<h2 class="_font-permanent-marker">Konservasi Penyu</h2>\').insertBefore(\'.slider-4 .slider-item-container .slider-item h5\')\r\nvar switchActive = $(\'.slider-4 .slider-item-container .slider-item:first-child\');\r\nvar itemActive = $(\'.slider-4 .slider-item-container .active\');\r\nitemActive.removeClass(\'active\');\r\nswitchActive.addClass(\'active\');\r\nvar itemClick = $(\'.slider-4 .slider-item-container .slider-item\');\r\nitemClick.click(function(){\r\nvar Active = $(\'.slider-4 .slider-item-container .active\');\r\nvar src = $(this).find(\'img\');\r\nvar image = src.attr(\'src\');\r\nvar srcActive = Active.find(\'img\');\r\nvar imageActive = srcActive.attr(\'src\');\r\nvar heading = $(this).find(\'h5\').text();\r\nvar headingActive = Active.find(\'h5\').text();\r\nconsole.log(heading);\r\nconsole.log(headingActive);\r\nsrc.attr(\'src\', imageActive);\r\nsrcActive.attr(\'src\', image);\r\nsrc.attr(\'src\', imageActive);\r\n$(this).find(\'h5\').html(headingActive);\r\nActive.find(\'h5\').html(heading);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 'slider-5', '.slider-5 .slider-item-container{\r\ndisplay:table;\r\nheight: 608px;\r\n}\r\n.slider-5 .slider-item-container .slider-item{\r\ntransition:all 1s;\r\ntransition-timing-function: ease-out;\r\n}\r\n.slider-5 .slider-item-container .active{\r\ndisplay:block !Important;\r\n}\r\n.slider-5 .slider-item-container .slider-item{\r\ndisplay:none;\r\nwidth:100%;\r\n    height: 100%;\r\n}\r\n.slider-5 .slider-item-container .slider-item img{\r\nwidth: 100%;\r\nposition:absolute;\r\n    object-fit: cover;\r\n    height: 100%;\r\n}\r\n.slider-5 .slider-item-container .slider-item.active{\r\nleft: 0;\r\nposition:absolute;\r\n}\r\n.slider-5 .slider-item-container .slider-item.before{\r\nposition: absolute;\r\nleft: -1366px;\r\ndisplay:block!important;\r\n}\r\n.slider-5 .slider-item-container .slider-item.after{\r\nposition: absolute;\r\nleft: 1366px;\r\ndisplay:block!important;\r\n}\r\n\r\n.slider-5 .slider-item-container .slider-item.active .slider-text-container{\r\n  visibility: visible;\r\n  opacity: 1;\r\n}\r\n.slider-5 .slider-item-container .slider-item .slider-text-container{\r\nposition:absolute;\r\ntop: 50%;\r\nmargin-top: -25px;\r\nleft:14%;\r\ncolor:#13274C;\r\n}\r\n.slider-5 .slider-item-container .slider-item .slider-text-container .slider-heading{\r\nmargin:0;\r\nfont-size:22px;\r\n}\r\n.slider-5 .slider-item-container .slider-item .slider-text-container .slider-description{\r\nmargin:0;\r\n}\r\n.slider-5 .slider-item-container .slider-item .slider-text-container h2{\r\nfont-size:40px;\r\n}\r\n.slider-5 .slider-item-container .slider-item .slider-text-container h4{\r\nfont-weight: 300;\r\nfont-size:20px;\r\n}\r\n.slider-5 .slider-item-container .slider-item .slider-text-container h3{\r\nfont-size:30px;\r\n}\r\n.slider-5 .slider-indicator{\r\nposition:absolute;\r\ntop: 50%;\r\nmargin-top: -130px;\r\nleft:46px;\r\n}\r\n.slider-5 .slider-indicator .slider-control-next{\r\ncursor:pointer;\r\n}\r\n.slider-5 .slider-indicator .slider-control-prev{\r\ncursor:pointer;\r\n}\r\n.slider-5 .slider-indicator ul{\r\npadding:0;\r\n}\r\n.slider-5 .slider-indicator ul li{\r\nlist-style:none;\r\nmargin: 30px 0px;\r\ncursor:pointer;\r\n}\r\n.slider-5 .slider-indicator ul li .fa{\r\ncolor:rgba(14,35,73,10%);\r\n}\r\n.slider-5 .slider-indicator ul li.active{\r\nborder: 2px solid #0E2349;\r\nborder-radius: 15px;\r\nwidth: 18px;\r\nheight: 18px;\r\nmargin-left: -4px;\r\ntext-align: center;\r\n}\r\n.slider-5 .slider-indicator ul li.active .fa{\r\ncolor: #0E2349;\r\nmargin-top: 1px;\r\n}\r\n.slider-5 .slider-indicator ul li.active .span-line{\r\nwidth:112px;\r\n    border-top: 1px solid black;\r\n    position: absolute;\r\n    top: 59%;\r\n    left: 18px;\r\n}', '<script type="text/javascript">\r\n$(document).ready(function(){\r\nitemActive = $(\'.slider-5 .slider-item-container .active\');\r\nitemIndexActive = itemActive.index();\r\n$(item.eq(itemIndexActive-1).addClass(\'before\'));\r\n$(item.eq(itemIndexActive+1).addClass(\'after\'));\r\n});\r\nvar itemActive = "";\r\nvar itemBefore = "";\r\nvar itemAfter = "";\r\nvar itemIndexActive = "";\r\nvar itemIndexBefore = "";\r\nvar itemIndexAfter = "";\r\nvar parent = $(\'.slider-5\');\r\nvar item = $(\'.slider-5 .slider-item-container .slider-item\');\r\nvar indicatorLength = "";\r\nfor (i = 0; i < item.length; i++) {\r\nindicatorLength += "<li><span class=\'fa fa-circle\'></span></li>";\r\n}\r\n\r\nvar indicator = \'<div class="slider-indicator"><div class="slider-control-prev"><span class="fa fa-chevron-up"></span></div><ul>\'+indicatorLength+\'</ul><div class="slider-control-next"><span class="fa fa-chevron-down"></span></div></div>\';\r\nparent.append(indicator);\r\nif($(\'.slider-5 .slider-item-container .active\')){\r\n$(\'.slider-5 .slider-indicator ul li\').eq(itemIndexActive).addClass(\'active\');\r\n$(\'.slider-5 .slider-indicator ul li\').append("<div class=\'.span-line\'></div>");\r\n}\r\n\r\nfunction addClass(){\r\nif(itemIndexAfter == 3){\r\n$(item.eq(itemIndexActive).addClass(\'before\'));\r\n$(item.eq(0).addClass(\'after\'));\r\n$(item.eq(itemIndexAfter).addClass(\'active\'));\r\n}else{\r\n$(item.eq(itemIndexActive).addClass(\'before\'));\r\n$(item.eq(itemIndexAfter+1).addClass(\'after\'));\r\n$(item.eq(itemIndexAfter).addClass(\'active\'));\r\n}\r\n}\r\n\r\nfunction addClassBefore(){\r\nif(itemIndexBefore == -1){\r\n$(item.eq(3).addClass(\'before\'));\r\n$(item.eq(itemIndexActive).addClass(\'after\'));\r\n$(item.eq(itemIndexBefore).addClass(\'active\'));\r\n}else{\r\n$(item.eq(itemIndexBefore-1).addClass(\'before\'));\r\n$(item.eq(itemIndexActive).addClass(\'after\'));\r\n$(item.eq(itemIndexBefore).addClass(\'active\'));\r\n}\r\n}\r\n\r\nfunction removeClass(){\r\nitemActive.removeClass(\'active\');\r\nitemBefore.removeClass(\'before\');\r\nitemAfter.removeClass(\'after\');\r\n}\r\n\r\n$(\'.slider-control-prev\').click(function() {\r\nitemActive = $(\'.slider-5 .slider-item-container .active\');\r\nitemBefore = $(\'.slider-5 .slider-item-container .before\');\r\nitemAfter = $(\'.slider-5 .slider-item-container .after\');\r\nitemIndexActive = itemActive.index();\r\nitemIndexBefore = itemBefore.index();\r\nitemIndexAfter = itemAfter.index();\r\nif(item.eq(itemIndexAfter+1) == 3){\r\n$(item.eq(3).addClass(\'before\'));\r\n$(item.eq(1).addClass(\'after\'));\r\n$(item.eq(0).addClass(\'active\'));\r\n}else{\r\nremoveClass();\r\n addClassBefore();\r\n}\r\nvar liActive = $(\'.slider-5 .slider-indicator ul li.active\');\r\nvar li = $(\'.slider-5 .slider-indicator ul li\');\r\nliActive.removeClass(\'active\');\r\nli.eq(itemIndexBefore).addClass(\'active\');\r\n});\r\n\r\n$(\'.slider-control-next\').click(function() {\r\nitemActive = $(\'.slider-5 .slider-item-container .active\');\r\nitemBefore = $(\'.slider-5 .slider-item-container .before\');\r\nitemAfter = $(\'.slider-5 .slider-item-container .after\');\r\nitemIndexActive = itemActive.index();\r\nitemIndexBefore = itemBefore.index();\r\nitemIndexAfter = itemAfter.index();\r\nif(item.eq(itemIndexAfter+1) == 3){\r\n$(item.eq(3).addClass(\'before\'));\r\n$(item.eq(1).addClass(\'after\'));\r\n$(item.eq(0).addClass(\'active\'));\r\n}else{\r\nremoveClass();\r\naddClass()\r\n}\r\nvar liActive = $(\'.slider-5 .slider-indicator ul li.active\');\r\nvar li = $(\'.slider-5 .slider-indicator ul li\');\r\nliActive.removeClass(\'active\');\r\nli.eq(itemIndexAfter).addClass(\'active\');\r\n});\r\n\r\n$(\'.slider-5 .slider-indicator ul li\').click(function() {\r\nitemActive = $(\'.slider-5 .slider-item-container .active\');\r\nitemBefore = $(\'.slider-5 .slider-item-container .before\');\r\nitemAfter = $(\'.slider-5 .slider-item-container .after\');\r\nremoveClass();\r\nconsole.log($(this).index());\r\n$(item.eq($(this).index()).addClass(\'active\'));\r\nitemActive = $(\'.slider-5 .slider-item-container .active\');\r\nitemIndexActive = itemActive.index();\r\n$(item.eq(itemIndexActive-1).addClass(\'before\'));\r\n$(item.eq(itemIndexActive+1).addClass(\'after\'));\r\nvar liActive = $(\'.slider-5 .slider-indicator ul li.active\');\r\nvar li = $(\'.slider-5 .slider-indicator ul li\');\r\nliActive.removeClass(\'active\');\r\nli.eq($(this).index()).addClass(\'active\');\r\n});\r\n\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 'slider-6', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 'slider-7', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `library_components` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.migrations: ~14 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_09_11_054233_create_group_table', 1),
	(4, '2019_09_11_054242_create_page_table', 1),
	(5, '2019_09_11_054304_create_public_table', 1),
	(6, '2019_09_11_054311_create_grid_table', 1),
	(7, '2019_09_11_054319_create_component_table', 1),
	(8, '2019_09_11_054340_create_library_component_table', 1),
	(9, '2019_09_11_054348_create_library_grid_table', 1),
	(10, '2019_09_11_054353_create_library_css_table', 1),
	(11, '2019_09_11_054359_create_library_javascript_table', 1),
	(12, '2019_09_23_094038_create_project_table', 1),
	(13, '2019_09_23_094217_create_section_table', 1),
	(14, '2019_09_23_123936_create_type_component_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.pages
DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `features_images` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_index` tinyint(4) NOT NULL DEFAULT 0,
  `group` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `published_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.pages: ~0 rows (approximately)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `name`, `features_images`, `description`, `is_index`, `group`, `parent`, `status`, `published_at`, `created_at`, `updated_at`) VALUES
	(1, 'home', NULL, NULL, 1, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.projects: ~0 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'peugeot indonesia', 1, NULL, NULL);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.publics
DROP TABLE IF EXISTS `publics`;
CREATE TABLE IF NOT EXISTS `publics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `pages` int(11) DEFAULT NULL,
  `projects` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.publics: ~0 rows (approximately)
/*!40000 ALTER TABLE `publics` DISABLE KEYS */;
INSERT INTO `publics` (`id`, `name`, `url`, `meta`, `parent`, `pages`, `projects`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'home', 'home', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">', NULL, 1, 1, 1, NULL, NULL);
/*!40000 ALTER TABLE `publics` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT 0,
  `pages` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.sections: ~8 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` (`id`, `name`, `html_id`, `html_class`, `sequence`, `pages`, `created_at`, `updated_at`) VALUES
	(1, 'section main banner', NULL, '_section-main-banner _container-full', 1, 1, NULL, NULL),
	(2, 'section preview product', NULL, '_section-preview-product _container-full', 2, 1, NULL, NULL),
	(3, 'section product video', NULL, '_section-product-video', 3, 1, NULL, NULL),
	(4, 'section offer', NULL, '_section-offer', 4, 1, NULL, NULL),
	(5, 'section body paint', NULL, '_section-body-paint', 5, 1, NULL, NULL),
	(6, 'section dealer', NULL, '_section-dealer', 6, 1, NULL, NULL),
	(7, 'section exibition', NULL, '_section-exibition _container-full', 7, 1, NULL, NULL),
	(8, 'section event promotion', NULL, '_section-event-promotion _container-full', 8, 1, NULL, NULL);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.type_component
DROP TABLE IF EXISTS `type_component`;
CREATE TABLE IF NOT EXISTS `type_component` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.type_component: ~12 rows (approximately)
/*!40000 ALTER TABLE `type_component` DISABLE KEYS */;
INSERT INTO `type_component` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'banner', NULL, NULL),
	(2, 'heading', NULL, NULL),
	(3, 'paragraph', NULL, NULL),
	(4, 'image', NULL, NULL),
	(5, 'video', NULL, NULL),
	(6, 'slider', NULL, NULL),
	(7, 'button', NULL, NULL),
	(8, 'link', NULL, NULL),
	(9, 'icon', NULL, NULL),
	(10, 'accordion', NULL, NULL),
	(11, 'breadcrumb', NULL, NULL),
	(12, 'card', NULL, NULL);
/*!40000 ALTER TABLE `type_component` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

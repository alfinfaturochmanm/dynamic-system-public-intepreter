<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CssStyleGuide extends Model
{

    protected $table = 'css_style_guide';

    protected $fillable = ['type_component', 'class', 'code'];

}
